.. luminance documentation master file, created by
   sphinx-quickstart on Fri Dec 22 12:12:25 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


`measuremelt`
=============

Helps with the work flow to identify hot melt (or anything else when providing
an appropriate detector function) in video records.
Measures area of melt visible in one or more frames of a video.

This originates in the *analysis of explosive magma-water interaction experiments.*

It is a somewhat rough method to quantify what happens when molten rock comes
into contact with ambient temperature water, and a video record exists of that
process that can be analyzed. The code came out of the effort to quantify, for
a given time (video frame), how much molten rock was from the container ejected
in which melt and water came into contact. Since it is difficult to put sensors
inside such a container the more remote video based technique was used.

For installation instructions and requirements read the :doc:`setup`.


.. toctree::
   :caption: Sections

   setup

   example

.. toctree::
   :caption: Modules
   :maxdepth: 1

   luminance

   count

   load


Indices and tables
------------------


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
