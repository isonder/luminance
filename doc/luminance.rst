`luminance`
-----------


.. automodule:: measuremelt.luminance

.. autofunction:: measuremelt.luminance.cumul_bright

.. autofunction:: measuremelt.luminance.luminance

.. autofunction:: measuremelt.luminance._cbright_chunk

.. autofunction:: measuremelt.luminance.cumul_bright_sequence

.. autofunction:: measuremelt.luminance.average_cbright

.. autofunction:: measuremelt.luminance.luminance_sequence

.. autofunction:: measuremelt.luminance.sigma_luminance

.. autofunction:: measuremelt.luminance.sigma_ldot

.. autofunction:: measuremelt.luminance.dldot

.. autofunction:: measuremelt.luminance.sigma_dldot

.. autoclass:: measuremelt.luminance.FilterProfile
   :members:
   :undoc-members:

.. autofunction:: measuremelt.luminance.filter_lum
